# **wss_auth - Authentication for Django Channels Using a Database Cluster -v2**

> **NOTE: THIS REPO IS DEPRECATED IN FAVOR OF THE LATEST MEDIUM ARTICLE FOR &quot;**[***Authentication for Django Channels Using a Database Cluster -v3***](https://medium.com/@matt_31770/authentication-for-django-channels-using-a-database-cluster-v3-772befc7faa1)**&quot;; YOU CAN FIND ITS COMPANION REPO HERE:** https://gitlab.com/sourcesolver1/django_wss_auth_v3

##### This is a git assembled for a second article in a series on medium.com titled "**Authentication for Django Channels using a Database Cluster**". **For more details, read the article at:**

> https://medium.com/@matt_31770/authentication-for-django-channels-using-a-database-cluster-v2-7250ef1bf667

##### This git includes a working demo that supports websocket authentication in Django. The code of this git is governed by an MIT license (see LICENSE.txt for details)
---
##### If you want to try a simple single app layout that uses asynchronous views, use the project contained in the folder "simple_async_wss_auth".
##### This v2/version 2 example supports a scalable implementation of a [postgres] database connection pool. It also shows a clean way to implement "raw SQL"/SQL driver code in Django *TODAY* while awaiting a stable release of the asynchronous Django ORM (slated for December 2024...)

Note that while both synchronous and asynchronous versions were provided for v1/version 1 of this project in the interests of guiding users new to an asynchronous view implementation, for v2 and v3 we will only furnish a performant asynchronous implementation.

SQL queries are done for postgres, though reworking them for other SQL queries, given their ANSI compliance, should be relatively easy to do. Revision of oermissioning and types in the CREATE TABLE statements would probably need to be done.

## Demo Installation Checklist

* Extract the project folder to be evaluated to your server (a **sub**folder of **/usr/local** is a good place...)

* Create a superuser for the demo project:

`
 python3 manage.py createsuperuser
`
`
* This second version automatically creates the tables it needs upon startup using the Django application database role defined in **settings.py**. This implies that the role would need to be the dtatbase owner -frequently the case in Django applications, though not always in configurations relying on a "least privilege" security model. For a more sophisticated permission model, .sql files can be found in a **"sql**" folder and can be loaded into a database administrative tool such as pgadmin and edited to suit, and then run to create the tables.

* Update the credentials in **settings.py** to use the ones needed for your database.

## Integration Checklist

If you want to integrate the WebSocket authentication from this demo project into your own project, do the following:

* If you have a database NOT owned by the Django database application role, edit the .sql files in the "**sql**" subfolder to use the appropriate database roles you are using in your existing project. These scripts were written for postgres, but can be adapted for the SQL of your choice. Run these scripts for the authentication table(s) to be set up.

* Integrate the settings in **settings.py** from the respective project you want to use into your own settings.py file.

* Copy **auth.py**, **wss_auth.py**, and **sql.py** (or integrate it, if you are using one of your own...) into the app subfolder of your project folder.

* In your **views.py**:

    * Import get_script_name, async_django_login as login, async_django_authenticate as authenticate, and async_authenticator as authenticator from auth.py

    * Import wss_auth_token from wss_auth.py

* Incorporate **asgi.py** into your own asgi.py (or add it to your main app subfolder below your project folder, if you don't already have one...) Note that this file module is what is invoked by the interactive Daphne script **daphne.sh**, or your Linux systemd service file **daphne.service**. If you want to set up Daphne as a systemd service, consult the comments in that .service file.

* Integrate **consumers.py** with your own consumers.py (or add it, if you don't have one already...)

* Integrate the JavaScript from the **wss_auth_demo.html** template into your templates, as needed.

* If you are using nginx as a proxy for your Django, integrate the settings in the **nginx.conf.fragment** file into your nginx site file. [**Recommended:** first make a backup of your existing nginx site file] See the comments in this file; they go into the details of using nginx as a proxy for a Daphne server. Do an "nginx -t" to confirm that your edits are correct, and then restart your nginx server (if you are using Linux systemd: "systemctl restart nginx"...)
