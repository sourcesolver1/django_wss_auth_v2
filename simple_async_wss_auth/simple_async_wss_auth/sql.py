#/usr/bin/python3
#
# sql.py
#
# (c) 2024 Matthew Hopard
# This code is furnished under an MIT license
# (see LICENSE.txt for details)

# This can vary from one SQL implementation to another;
# this is the way it is calculated in postgres...
ELAPSED_SECONDS="EXTRACT('EPOCH' FROM {date_time2})-EXTRACT('EPOCH' FROM {date_time1})"

import sys
import os

from psycopg import connect
import psycopg_pool
import inspect
import asyncio
import atexit

def err (message=''):
    sys.stderr.write (str(message)+'\n')
    sys.stderr.flush () # err

try: # relative import...
     from . import settings

except: # must be a unit test...
        import settings

POSTGRES_PORT=5432

# defaults...
SQL_POOL_MIN_SIZE=getattr(settings, 'SQL_POOL_MIN_SIZE', 4)
SQL_POOL_MAX_SIZE=getattr(settings, 'SQL_POOL_MAX_SIZE', None)

def if_zero_length_string(string, default_if_zero_length):
    if string=='':
       return default_if_zero_length

    return string # if_zero_length_string

def elapsed_seconds(date_time1, date_time2):
    global ELAPSED_SECONDS

    return ELAPSED_SECONDS.format(date_time1='date_time', date_time2='NOW()') # elapsed_seconds

TABLES={# Views are treated as tables;
        # list tables in this dictionary BEFORE the views referencing them...

        'wss_auth': {'CREATE': '''CREATE TABLE IF NOT EXISTS wss_auth
(id BigSerial PRIMARY KEY NOT NULL,
 date_time Timestamp With Time Zone NOT NULL DEFAULT NOW(),
 project VarChar(255),
 app VarChar(255),
 username VarChar(255) NOT NULL,
 token VarChar(255) NOT NULL,
 socket_created_seconds Int DEFAULT NULL,
 ip Inet
); -- wss_auth

CREATE INDEX IF NOT EXISTS wss_auth_date_time ON wss_auth(date_time);
CREATE INDEX IF NOT EXISTS wss_auth_project ON wss_auth(project);
CREATE INDEX IF NOT EXISTS wss_auth_app ON wss_auth(app);
CREATE INDEX IF NOT EXISTS wss_auth_username ON wss_auth(username);
CREATE INDEX IF NOT EXISTS wss_auth_token ON wss_auth(token);
CREATE INDEX IF NOT EXISTS wss_auth_ip ON wss_auth(ip);

--ALTER TABLE wss_auth OWNER TO TRUSTED_OWNER_ROLE;
--GRANT ALL ON TABLE wss_auth TO TRUSTED_OWNER_ROLE;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE wss_auth TO {django_app_database_role};
GRANT USAGE, SELECT, UPDATE ON SEQUENCE wss_auth_id_seq TO {django_app_database_role};
''', # CREATE

                     'INSERT': '''INSERT INTO wss_auth (project, app, username, token, ip)
VALUES (%(project)s, %(app)s, %(username)s, %(token)s, %(ip)s)
''', # INSERT

                     # psycopg3 bug? It appears to get confused when you pass a named parameter
                     # to an INTERVAL expression, so we do this instead and use a string .format
                     # Since the parameter comes from the settings.py file, which is presumably
                     # a trusted input, this is safe to do...
                    'SELECT': '''SELECT id, socket_created_seconds
FROM wss_auth
WHERE username=%(username)s AND token=%(token)s AND
      date_time>=NOW()-INTERVAL '{seconds_window}'
ORDER BY date_time DESC
LIMIT 1
''', # SELECT

                    'UPDATE': '''UPDATE wss_auth
SET socket_created_seconds={elapsed_seconds}
WHERE id=%(id)s
'''.format(elapsed_seconds=elapsed_seconds('date_time', 'NOW()')), # UPDATE

                     # psycopg3 bug? It appears to get confused when you pass a named parameter
                     # to an INTERVAL expression, so we do this instead and use a string .format
                     # Since the parameter comes from the settings.py file, which is presumably
                     # a trusted input, this is safe to do...
                     'DELETE': '''DELETE FROM wss_auth
WHERE date_time<NOW()-INTERVAL '{max_wss_auth_age_interval}'
''' # DELETE
                    }, # wss_auth

        'wss_auth_redacted': {'CREATE': '''CREATE OR REPLACE VIEW wss_auth_redacted
AS
SELECT id, date_time, project, app, username, socket_created_seconds, ip
       FROM wss_auth;

--ALTER VIEW wss_auth_redacted OWNER TO TRUSTED_OWNER_ROLE;
--GRANT SELECT ON wss_auth_redacted TO ADMINS_GROUP_ROLE, DEVELOPERS_GROUP_ROLE;
''' # CREATE
                             }, # wss_auth_redacted
 
        'wss_auth_fail_log': {'CREATE': '''CREATE TABLE IF NOT EXISTS wss_auth_fail_log
(id BigSerial PRIMARY KEY NOT NULL,
 date_time Timestamp With Time Zone NOT NULL DEFAULT NOW(),
 project VarChar(255),
 username VarChar(255) NOT NULL,
 token VarChar(255) NOT NULL,
 ip Inet
); -- wss_auth_fail_log

CREATE INDEX IF NOT EXISTS wss_auth_fail_log_date_time ON wss_auth_fail_log(date_time);
CREATE INDEX IF NOT EXISTS wss_auth_fail_log_project ON wss_auth_fail_log(project);
CREATE INDEX IF NOT EXISTS wss_auth_fail_log_username ON wss_auth_fail_log(username);
CREATE INDEX IF NOT EXISTS wss_auth_fail_log_token ON wss_auth_fail_log(token);
CREATE INDEX IF NOT EXISTS wss_auth_fail_log_ip ON wss_auth_fail_log(ip);

--ALTER TABLE wss_auth_fail_log OWNER TO TRUSTED_OWNER_ROLE;
--GRANT ALL ON TABLE wss_auth_fail_log TO TRUSTED_OWNER_ROLE;
GRANT INSERT, SELECT, DELETE ON TABLE wss_auth_fail_log TO {django_app_database_role};
GRANT USAGE, SELECT, UPDATE ON SEQUENCE wss_auth_fail_log_id_seq TO {django_app_database_role};
''', # CREATE

                              'INSERT': '''INSERT INTO wss_auth_fail_log (project, username, token, ip)
VALUES (%(project)s, %(username)s, %(token)s, %(ip)s)
''', # INSERT

                              # psycopg3 bug? It appears to get confused when you pass a named parameter
                              # to an INTERVAL expression, so we do this instead and use a string .format
                              # Since the parameter comes from the settings.py file, which is presumably
                              # a trusted input, this is safe to do...
                              'DELETE': '''DELETE FROM wss_auth_fail_log
WHERE date_time<NOW()-INTERVAL '{max_wss_auth_fail_log_age_interval}'
''' # DELETE
                             }, # wss_auth_fail_log

        'wss_auth_fail_log_redacted': {'CREATE': '''CREATE OR REPLACE VIEW wss_auth_fail_log_redacted
AS
SELECT id, date_time, project, username, ip
       FROM wss_auth_fail_log;

--ALTER VIEW wss_auth_fail_log_redacted OWNER TO TRUSTED_OWNER_ROLE;
--GRANT SELECT ON wss_auth_fail_log_redacted TO ADMINS_GROUP_ROLE, DEVELOPERS_GROUP_ROLE;
''' # CREATE
                                      } # wss_auth_fail_log_redacted
       } # TABLES

def get_column_indexes(cursor):
    column_indexes={}
    index=0
    for description in cursor.description:
        column_indexes[description[0]]=index
        index+=1

    return column_indexes # get_column_indexes

# pick up settings from settings.py...
DATABASE={'host': if_zero_length_string(settings.DATABASES['default']['HOST'], 'localhost'),
          'port': if_zero_length_string(settings.DATABASES['default']['PORT'], str(POSTGRES_PORT)),
          'dbname': settings.DATABASES['default']['NAME'],
          'user': settings.DATABASES['default']['USER'],
          'pw': settings.DATABASES['default']['PASS'+'WORD']
         } # DATABASE

dsn='host='+DATABASE['host']+' port='+DATABASE['port']+' '+ \
    'dbname='+DATABASE['dbname']+' '+ \
    'user='+DATABASE['user']+' pass'+'word='+DATABASE['pw']

pool=None # Failsafe Default...
event_loop=None
MODULE=os.path.basename(__file__)

def create_tables(database_role):
    'we do this synchronously at startup to insure that tables exist before we access them...'
    global dsn, MODULE

    failed=False
    try: # database connection...
         with connect(dsn) as database:
              for table in TABLES:
                  if 'CREATE' in table:
                     create_table=table['CREATE'].format(
django_app_database_role=database_role
                                                        )
                     try: # executing the cursor...
                          with database.cursor() as cursor:
                               cursor.execute (create_table)
                               database.commit ()

                     except Exception as exception:
                            err (MODULE+'; CREATE TABLE '+table+': '+str(exception))
                            failed=True

    except: # database connection...
            err (MODULE+'; connection string defined in settings.py failed: '+str(exception))
            failed=True

    return not failed # create_tables

def dealloc_pool ():

    async def dealloc():
              global pool, MODULE

              failed=False # Failsafe Default...
              err (MODULE+': deallocating Connection Pool...')
              if not (pool is None):
                 try:
                      await pool.close ()

                 except Exception as exception:
                        err (MODULE+'; dealloc: '+str(exception))
                        failed=True

                 pool=None

              return not failed # dealloc

    if not (event_loop is None):
       event_loop.run_until_complete (dealloc()) # dealloc_pool

async def create_pool():
          global pool, dsn, SQL_POOL_MIN_SIZE, SQL_POOL_MAX_SIZE, MODULE

          err (MODULE+': allocating Connection Pool...')
          try:
               # psycopg3 bug? "open" argument does not appear to work;
               pool=psycopg_pool.AsyncConnectionPool(dsn,
                                                     min_size=SQL_POOL_MIN_SIZE,
                                                     max_size=SQL_POOL_MAX_SIZE
                                                    ) ##??, open=True)
               await pool.open () # so we explicitly do this instead...
               await pool.wait ()
               err ('Connection Pool allocated...')
               atexit.register (dealloc_pool)

          except Exception as exception:
                 err (MODULE+': '+exception)
                 return False # create_pool

          return True # create_pool

if pool is None: # first time?
   create_tables (database_role=DATABASE['user'])
   event_loop=asyncio.new_event_loop()
   event_loop.run_until_complete (create_pool())
