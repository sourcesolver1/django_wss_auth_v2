#!/usr/bin/python3
#
# generate_new_secret_key.py
#
# (c) 2024 Matthew Hopard
# This code is furnished under an MIT license
# (see LICENSE.txt for details)

from django.core.management.utils import get_random_secret_key  

def quotes(text):
    return "'"+text+"'"

secret_key='SECRET_KEY='+quotes(get_random_secret_key())
settings=''
had_key=False
with open('settings.py', 'r') as lines:
     for line in lines.readlines():
         if line.lstrip().startswith('SECRET_KEY='):
            line=secret_key+'\n'
            had_key=True

         settings+=line

if not had_key:
   settings+=secret_key+'\n'

with open('settings.py', 'w') as lines:
     lines.write (settings)
