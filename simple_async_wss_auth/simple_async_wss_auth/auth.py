#!/usr/bin/python3
#
# auth.py
#
# general purpose authentication view endpoint functions
# used for a django login that can be imported to views.py
#
# (c) 2024 Matthew Hopard
# This code is furnished under an MIT license
# (see LICENSE.txt for details)

import sys
import json
import pprint

from django.shortcuts import render, redirect, reverse
from django.http import HttpResponseNotAllowed, JsonResponse
#
# To cut down on the sync vs. async confusion and to better diffentiate
# one from the other, we will use the explicit prefixes "sync_" and "async_"
# (as opposed to the less self-documenting convention of just the letter "a"
#  for async before a function call...); and to make it clear when calls are
# standard Django ones, we will also use a qualifier of "django_".
# So, for example, we have "sync_django_get_user" for the standard synchronous
# django.contrib.auth call "get_user"; "async_django_authenticate" for the
# asynchronous implementation of Django 5's "authenticate"; and so forth.
# In the views.py we then import the ones we need, be it synchronous or
# asynchronous. Feel free to adjust the calls to use the names which will be
# deemed standard in Django 5. For now, these will work with lesser versions
# of Django, and are a bit easier to follow since they clarify when it's a
# standard Django call and whether it's synchronous or asynchronous...
from django.contrib.auth import get_user as sync_django_get_user, \
                                authenticate as sync_django_authenticate, \
                                login as sync_django_login
from django.contrib import messages
from asgiref.sync import sync_to_async

try: # a Django relative import...
     from . import settings
     from .forms import LoginForm, SignupForm

except: # in case a module is importing this one for an interactive Unit Test...
        import settings
        from forms import LoginForm, SignupForm

# To not give an attacker info for reconnaisance
# (with the exception for when an account is deactivated...),
# we respond with no info: {}
AUTH_FAIL_JSON={}

# Defaults...
MAX_CREDENTIALS_LENGTH=getattr(settings, 'MAX_CREDENTIALS_LENGTH', 255)

def err (message=''):
    sys.stderr.write (str(message)+'\n')
    sys.stderr.flush () # err

def print_object (object):
    pprint.pprint (vars(object))

def bad_http_method(request, *methods):
    if not (request.method in methods):
       return HttpResponseNotAllowed(methods)

def first_param(params_or_param):
    if isinstance(params_or_param, list):
       return params_or_param[0] # first_param

    return params_or_param # first_param

def get_parameter(request, parameter_name):
    if request.method=='GET':
       return first_param(request.GET.get(parameter_name, None)) # get_parameter

    if request.method=='POST':
       return first_param(request.POST.get(parameter_name, None)) # get_parameter

def get_origin(request):
    'takes into account if the request was proxied...'
    if 'HTTP_X_FORWARDED_PROTO' in request.META:
       scheme=request.META['HTTP_X_FORWARDED_PROTO']

    else:
          scheme=request.scheme

    if 'HTTP_X_FORWARDED_PORT' in request.META:
       port=request.META['HTTP_X_FORWARDED_PORT']

    else:
          port=request.get_port()

    origin=scheme+'://'+request.get_host()
    if not ((scheme=='https' and port=='443') or
            (scheme=='http' and port=='80')
           ):
       origin+=':'+request.get_port()

    return origin # get_origin

def get_script_name(request):
    'used to determine the proxy path prefix, if you are proxied by nginx or another caching proxy...'
    if 'HTTP_X_SCRIPT_NAME' in request.META:
       return request.META['HTTP_X_SCRIPT_NAME'] # get_script_name

    script_name=request.META.get('SCRIPT_NAME', '')
    if script_name!='':
       return script_name # get_script_name

    return getattr(settings, 'SCRIPT_NAME', '') # get_script_name

def get_script_path(request):
    script_name=get_script_name(request)
    if script_name=='':
       return '' # get_script_path

    return script_name+'/' # get_script_path

def get_url_prefix(request):
    script_name=get_script_name(request)
    if script_name=='':
       script_name='/'

    if script_name!='/':
       if not script_name.startswith('/'):
          script_name='/'+script_name

       if not script_name.endswith('/'):
          script_name=script_name+'/'

    return get_origin(request)+script_name

async def async_django_get_user(request):
          return await sync_to_async(sync_django_get_user)(request)

async def async_django_authenticate(request, username, password):
          return await sync_to_async(sync_django_authenticate)(request,
                                                               username=username,
                                                               password=password
                                                              )

async def async_django_login(request, user):
          return await sync_to_async(sync_django_login)(request, user)

def sync_authenticator(request):
    'endpoint for authentication using AJAX posts...'
    global AUTH_FAIL_JSON

    bad=bad_http_method(request, 'POST')
    if not (bad is None):
       return JsonResponse(AUTH_FAIL_JSON) # sync_authenticator

    if len(request.body)>settings.MAX_CREDENTIALS_LENGTH: # somebody is being a comedian...
       err ('auth: request body length>'+str(settings.MAX_CREDENTIALS_LENGTH)+'...')
       return JsonResponse(AUTH_FAIL_JSON) # sync_authenticator

    try:
         credentials=json.loads(request.body)

    except Exception as exception:
           err ('auth: '+str(exception))
           return JsonResponse(AUTH_FAIL_JSON) # sync_authenticator

    if not ('username' in credentials and 'password' in credentials):
       err ('auth: missing both username and password from credentials...')
       return JsonResponse(AUTH_FAIL_JSON) # async_auth

    user=sync_django_authenticate(request,
                                  username=credentials['username'],
                                  password=credentials['password']
                                 )
    if user is None: # didn't authenticate...
       return JsonResponse(AUTH_FAIL_JSON) # sync_authenticator

    if not user.is_active:
       return JsonResponse({'authenticated': False,
                            'message': 'Disabled account...'
                           }
                          ) # sync_authenticator

    return JsonResponse({'authenticated': True, 'message': None}) # sync_authenticator

async def async_authenticator(request):
          'endpoint for authentication using AJAX posts...'
          global AUTH_FAIL_JSON

          bad=bad_http_method(request, 'POST')
          if not (bad is None):
             return JsonResponse({}) # async_authenticator

          if len(request.body)>settings.MAX_CREDENTIALS_LENGTH: # somebody is being a comedian...
             err ('auth: request body length>'+str(settings.MAX_CREDENTIALS_LENGTH)+'...')
             return JsonResponse({}) # async_authenticator

          try:
               credentials=json.loads(request.body)

          except Exception as exception:
                 err ('auth: '+str(exception))
                 return JsonResponse({}) # async_authenticator

          if not ('username' in credentials and 'password' in credentials):
             err ('auth: missing both username and password from credentials...')
             return JsonResponse({}) # async_authenticator

          user=await async_django_authenticate(request,
                                               username=credentials['username'],
                                               password=credentials['password']
                                              )
          if user is None: # didn't authenticate...
             return JsonResponse({}) # async_authenticator

          if not user.is_active:
             return JsonResponse({'authenticated': False,
                                  'message': 'Disabled account...'
                                 }
                                ) # async_authenticator

          return JsonResponse({'authenticated': True, 'message': None}) # async_authenticator

def sync_login(request):
    'a standard view function for logging in; import this to views.py if you want to use it...'
    messages.get_messages(request).used=True # clear...

    bad=bad_http_method(request, 'GET', 'POST')
    if not (bad is None):
       return bad # wss_auth_demo

    next=get_parameter(request, 'next')
    if request.method=='GET':
       form=LoginForm()

    else: # POST...
          form=LoginForm(data=request.POST)
          if not form.is_valid():
             messages.error (request, 'Invalid login credentials...')

          else: # form.is_valid():
                username=form.cleaned_data['username']
                pw=form.cleaned_data['password']
                user=sync_django_authenticate(request,
                                              username=username, password=pw
                                             )
                if (not (user is None)) and not user.is_active:
                   messages.error (request, 'Inactive user account...')

                elif user is None or not user.is_authenticated:
                     messages.error (request, 'Invalid login credentials...')

                else:
                      sync_django_login (request, user)
                      return redirect(next) # sync_login

    return render(request, 'registration/login.html',
                  {'form': form,
                   'next': next
                  }
                 ) # sync_login

async def async_login(request):
          'a standard view function for logging in; import this to views.py if you want to use it...'
          messages.get_messages(request).used=True # clear...

          bad=bad_http_method(request, 'GET', 'POST')
          if not (bad is None):
             return bad # wss_auth_demo

          next=get_parameter(request, 'next')
          if request.method=='GET':
             form=LoginForm()

          else: # POST...
                form=LoginForm(data=request.POST)
                if not form.is_valid():
                   messages.error (request, 'Invalid login credentials...')

                else: # form.is_valid():
                      username=form.cleaned_data['username']
                      pw=form.cleaned_data['password']
                      # before authenticating, check to see if the account is inactive
                      # and pass back that specific error if it is...
                      user=await async_django_authenticate(request,
                                                           username=username, password=pw
                                                          )
                      if (not (user is None)) and not user.is_active:
                         messages.error (request, 'Inactive user account...')

                      elif user is None or not user.is_authenticated:
                           messages.error (request, 'Invalid login credentials...')

                      else:
                            await async_django_login (request, user)
                            return redirect(next) # async_login

          return render(request, 'registration/login.html',
                        {'form': form,
                         'next': next
                        }
                       ) # async_login

def sync_signup(request):
    'a standard view function for signing up; import this to views.py if you want to use it...'
    bad=bad_http_method(request, 'GET', 'POST')
    if not (bad is None):
       return bad # sync_signup

    if request.method=='GET':
       form=SignupForm()

    else: # POST
          form=SignupForm(request.POST)
          if form.is_valid():
             username=form.cleaned_data.get('username')
             pw=form.cleaned_data.get('password1')
             user=sync_authenticate(request,
                                    username=username, password=pw
                                   )
             if (not user is None):
                sync_django_login (request, user)
                return redirect('login') # sync_signup

    return render(request, 'signup.html', {'form': form}) # sync_signup

async def async_signup(request):
          'a standard view function for signing up; import this to views.py if you want to use it...'
          bad=bad_http_method(request, 'GET', 'POST')
          if not (bad is None):
             return bad # async_signup

          if request.method=='GET':
             form=UserCreationForm()

          else: # POST
                form=UserCreationForm(request.POST)
                if form.is_valid():
                   username=form.cleaned_data.get('username')
                   pw=form.cleaned_data.get('password')
                   user=await async_authenticate(request,
                                                 username=username, password=pw
                                                )
                   if not user is None:
                      await async_django_login (request, user)
                      return redirect('login') # async_signup

          return render(request, 'signup.html', {'form': form}) # async_signup
