// wss_auth.js
//
// "vanilla.js" functions...
//
// (c) 2024 Matthew Hopard
// This code is furnished under an MIT license
// (see LICENSE.txt for details)

const ASYNC=true,
      HTTP_OK=200;

function get_request (url,
                      callback_function,
                      headers, // can be omitted...
                      response_type // 'html', 'json'; default='html
                     ) // Does an AJAX get...
{
 function onload ()
 {callback_function (request.status, request.response);
 } // function onload ()

 var request=new XMLHttpRequest();
 request.open ('GET', url, ASYNC);
 if (headers!==undefined)
    for (header in headers)
        request.setRequestHeader (header, headers[header]);

 request.responseType=(response_type===undefined?'html':response_type);
 request.onload=onload;
 request.send ();
} // function get_request (url, callback_function, headers, response_type)
