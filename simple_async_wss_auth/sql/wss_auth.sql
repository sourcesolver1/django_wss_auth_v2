-- wss_auth.sql
--
-- (c) 2024 Matthew Hopard
-- This code is furnished under an MIT license
-- (see LICENSE.txt for details)
--
-- You will first need to designate roles in postgres
-- for the following template expressions in this script
-- (you can replace the template expressions in this script
--  with the role names you decide on...)
--
-- TRUSTED_OWNER_ROLE: A database user role that is used for setup of sensitive info
--
-- DJANGO_APP_ROLE: The database role used by a Django app needing to perform
--                  websocket authentication
--
-- ADMINS_GROUP_ROLE: used to grant admin access to the "wss_auth_redacted" view
--                    (omits the "token" column...)
--
-- DEVELOPERS_GROUP_ROLE: used to grant developer access to the "wss_auth_redacted" view
--                        (omits the "token" column...)
--

CREATE TABLE IF NOT EXISTS wss_auth
(id BigSerial PRIMARY KEY NOT NULL,
 date_time Timestamp With Time Zone NOT NULL DEFAULT NOW(),
 project VarChar(255),
 app VarChar(255),
 username VarChar(255) NOT NULL,
 token VarChar(255) NOT NULL,
 socket_created_seconds Int DEFAULT NULL,
 ip Inet
); -- wss_auth

-- If there will be A LOT of records, you can opt to use Timescale
--
--      https://docs.timescale.com/self-hosted/latest
--
-- Pros and Cons:
--
--      https://croz.net/news/timescale-part-two/#:~:text=are%20a%20match.-,Pros%20and%20Cons,-Let%E2%80%99s%20check%20out
--
CREATE INDEX IF NOT EXISTS wss_auth_date_time ON wss_auth(date_time);
CREATE INDEX IF NOT EXISTS wss_auth_project ON wss_auth(project);
CREATE INDEX IF NOT EXISTS wss_auth_app ON wss_auth(app);
CREATE INDEX IF NOT EXISTS wss_auth_username ON wss_auth(username);
CREATE INDEX IF NOT EXISTS wss_auth_token ON wss_auth(token);
CREATE INDEX IF NOT EXISTS wss_auth_ip ON wss_auth(ip);

ALTER TABLE wss_auth OWNER TO TRUSTED_OWNER_ROLE;
GRANT ALL ON TABLE wss_auth TO TRUSTED_OWNER_ROLE;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE wss_auth TO DJANGO_APP_ROLE;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE wss_auth_id_seq TO DJANGO_APP_ROLE;

CREATE OR REPLACE VIEW wss_auth_redacted
AS
SELECT id, date_time, project, app, username, socket_created_seconds, ip
       FROM wss_auth;

ALTER VIEW wss_auth_redacted OWNER TO TRUSTED_OWNER_ROLE;
GRANT SELECT ON wss_auth_redacted TO ADMINS_GROUP_ROLE, DEVELOPERS_GROUP_ROLE;
