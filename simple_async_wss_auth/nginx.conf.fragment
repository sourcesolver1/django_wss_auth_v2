# If you are running nginx as a proxy to your Daphne server,
# integrate these directives into your nginx config file
# (on linux: /etc/nginx/nginx.conf)
#
# Do an "nginx -t" to check that the configuration is OK,
# and then restart nginx.
#

http {
      # The following logging directives would go in the http block
      # of your global nginx config file (on linux: /etc/nginx/nginx.conf)
      # Be sure to comment out your old "access_log" directive.

      log_format combined
                 '$remote_addr - $remote_user [$time_local] '
                 '"$request" $status $body_bytes_sent '
                 '"$http_referer" "$http_user_agent"';

      log_format combined_no_query
                 '$remote_addr - $remote_user [$time_local] '
                 '"$uri" $status $body_bytes_sent '
                 '"$http_referer" "$http_user_agent"';

      log_format combined_with_host_and_time
                 '$remote_addr - $remote_user [$time_local] '
                 '"request" $status $body_bytes_sent '
                 '"$http_referer" "$http_user_agent" $host $request_time';

      log_format combined_with_host_and_time_no_query
                 ''$remote_addr - $remote_user [$time_local] '
                 '"$uri" $status $body_bytes_sent '
                 '"$http_referer" "$http_user_agent" $host $request_time';

      # Log formats with a "..._no_query" suffix will suppress logging
      # of query parameters for ALL requests: wss:, https:...
      # use one of the ones listed above that captures the info you want.
      #
      access_log /var/log/nginx/access.log combined_with_host_and_time_no_query;

      # Another approach would be to disable logging of ALL wss: requests so that
      # query parameters for them do not appear in nginx's access.log,
      # while those for https: requests will still log query parameters.
      # The Daphne server log can still have them logged,
      # but we make sure that is only accessible by a dedicated "daphne"
      # linux account used by the Daphne server's systemd service.
      #
      # Comment the access_log directive above and uncomment
      # the following directives if you would prefer that...
      #
#      map $scheme $not_wss {wss  0;
#                            default 1;
#                           } # map $scheme $not_wss
#      access_log /var/log/nginx/access.log combined_with_host_and_time if=$not_wss;

     } # http

server {
        # The following location block would go into the server block of your
        # sites-available conf file.
        #
        # The /wss_auth/ endpoint is just for demo purposes, to insure it
        # does not collide with other nginx endpoints you might be using while
        # evaluating this demo; adjust it to suit for your testing and
        # production needs. Make sure this /wss_auth/ URL pattern PRECEDES
        # any other more general ones to insure that it gets applied
        # before them.
        #
        # This wss_auth prefix (called the "script_name" in proxy-speak...)
        # redirects all URL paths beginning with that folder to your Daphne server
        # according to the proxy_pass directive below.
        # If you want to have _all_ URLs sent to your Daphne
        # server then use the directive "location ^~ / {..."
        # and omit the "proxy_set_header X-Script-Name" directive...
        location ^~ /wss_auth/ {
                                # You base the following directive on the
                                # URL prefix path; that is what is known in
                                # proxy-speak as the "script_name", and it
                                # distinguishes this proxy from others you
                                # might (eventually?) be running at various
                                # URL prefixes. Omit this directive if you
                                # want ALL URLs sent to your Daphne server
                                # and change the location directive above
                                # to "location ^~ / {"
                                proxy_set_header X-Script-Name wss_auth;

                                # If you are running Daphne on a dedicated server
                                # separate from the nginx server, adjust the 127.0.0.1
                                # address accordingly and be sure the network port
                                # is open on its firewall.
                                # 8443 here is the network port of the Daphne server 
                                # instance; adjust as needed to reflect your Daphne
                                # server's port.
                                #
                                proxy_pass http://127.0.0.1:8443$request_uri;

                                # The remaining proxy_... directives here can be placed
                                # in the file /etc/nginx/proxy_params and included
                                # here if you are going to proxy multiple endpoints;
                                # re-include the parameters for each proxy location
                                # by specifying:
                                #
                                #      include /etc/nginx/proxy_params;

                                # The original IP address of the network client,
                                # as opposed to localhost/127.0.0.1
                                # (since Daphne usually receives proxied
                                # requests from the same server that
                                # nginx is running on...)
                                proxy_set_header X-Real-IP $remote_addr;

                                # This tells Daphne the original hostname
                                # used in the URL (as opposed to localhost,
                                # when routed to a local Daphne server instance...)                                                           proxy_set_header Host $http_host;

                                # This tells Daphne if the original URL
                                # was https or http...
                                proxy_set_header X-Forwarded-Proto $scheme;

                                # This tells Daphne if the original network port
                                # of the URL (usually 443 for https...)
                                proxy_set_header X-Forwarded-Port $server_port;

                                proxy_http_version 1.1;
                                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                                proxy_set_header Upgrade $http_upgrade;
                                proxy_set_header Connection "Upgrade";
                               } # location ^~ /wss_auth/

       } # server
